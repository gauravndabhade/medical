package com.db.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.db.medical.service.MedicalRepresentativeService;
import com.db.medical.model.MedicalRepresentative;

@RestController
public class MedicalRepresentativeController {

	@Autowired
	private MedicalRepresentativeService mrService;
	
	@PostMapping("/create")
	public ResponseEntity<MedicalRepresentative> createMedicalRepresentative(@RequestBody MedicalRepresentative medicalRepresentative) {
		try {
			MedicalRepresentative mr = mrService.create(medicalRepresentative); 
			return new ResponseEntity<MedicalRepresentative>(mr, HttpStatus.CREATED);	
		}
		catch(Exception e) {
			// System.out.println("Error :" + e.printStackTrace());
			return new ResponseEntity<MedicalRepresentative>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<List<MedicalRepresentative>> allMedicalRepresentative() {
		try {
			List<MedicalRepresentative> allmr = mrService.findAll();
			return new ResponseEntity<List<MedicalRepresentative>>(allmr, HttpStatus.OK);
		}
		catch(Exception e) {
			// System.out.println("Error :" + e.printStackTrace());
			return new ResponseEntity<List<MedicalRepresentative>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<MedicalRepresentative> updateMedicalRepresentative(@RequestBody MedicalRepresentative medicalRepresentative) {
		try {
			MedicalRepresentative mr = mrService.update(medicalRepresentative);
			return new ResponseEntity<MedicalRepresentative>(mr, HttpStatus.OK);
		}
		catch(Exception e) {
			// System.out.println("Error :" + e.printStackTrace());
			return new ResponseEntity<MedicalRepresentative>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<Object> deleteMedicalRepresentative(@RequestParam String idMR)	{
		mrService.delete(idMR);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/getMRDetailsById")
	public ResponseEntity<MedicalRepresentative> getMRDetailsById(@RequestParam String idMR) {
		try {			
			MedicalRepresentative mr = mrService.getDetailByID(idMR);
			return  new ResponseEntity<MedicalRepresentative>(mr, HttpStatus.OK);
		}
		catch(Exception e) {
			// System.out.println("Error :" + e.printStackTrace());
			return new ResponseEntity("Something went wrong",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}