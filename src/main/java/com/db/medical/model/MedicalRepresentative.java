package com.db.medical.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class MedicalRepresentative {
	@Id
	String id;
	String name;
	List<String> drugs;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getDrugs() {
		return drugs;
	}
	public void setDrugs(List<String> drugs) {
		this.drugs = drugs;
	}
	public MedicalRepresentative(String name, List<String> drugs) {
		super();
		this.name = name;
		this.drugs = drugs;
	}
	@Override
	public String toString() {
		return "MedicalRepresentative [name=" + name + ", drugs=" + drugs + "]";
	}
}
