package com.db.medical.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.medical.model.MedicalRepresentative;
import com.db.medical.repository.MedicalRepresentativeRepository;

@Service
public class MedicalRepresentativeService {
	
	@Autowired
	private MedicalRepresentativeRepository mrRepository;

	public MedicalRepresentative create(MedicalRepresentative medicalRepresentative) {
		return mrRepository.save(medicalRepresentative);
	}
	
	public List<MedicalRepresentative> findAll() {
		return mrRepository.findAll();
	}
	
	public MedicalRepresentative update(MedicalRepresentative medicalRepresentative) {
		 MedicalRepresentative mr = mrRepository.findByName(medicalRepresentative.getName());
		 if (null == mr ) {
			 // Add new MR
			 return mrRepository.save(medicalRepresentative);
		 } else {
			 // Update drugs list for MR
			 List<String> drugs = mr.getDrugs();
			 for (String drug : medicalRepresentative.getDrugs()) {
				 if (!drugs.contains(drug)) {
					 drugs.add(drug);
				 }
		     }
			 mr.setDrugs(drugs);
			 return mrRepository.save(mr);
		 }
	}
	
	public void delete(String id) {
		mrRepository.deleteById(id);
	}
	
	public MedicalRepresentative getDetailByID(String id) throws Exception {
		MedicalRepresentative mr = mrRepository.findAllById(id);
		if (null == mr) {
			throw new Exception("Medical Representative not found");
		} else {			
			return mr;
		}
	}
}
