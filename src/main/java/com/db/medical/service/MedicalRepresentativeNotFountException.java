package com.db.medical.service;

class MedicalRepresentativeNotFountException extends Exception { 
    public MedicalRepresentativeNotFountException(String errorMessage) {
        super(errorMessage);
    }
}