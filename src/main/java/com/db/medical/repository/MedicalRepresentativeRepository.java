package com.db.medical.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.db.medical.model.MedicalRepresentative;

@Repository
public interface MedicalRepresentativeRepository extends MongoRepository<MedicalRepresentative, String>{

	public MedicalRepresentative findByName(String name);
	public MedicalRepresentative findAllById(String id);
}
